//
//  SBIGameObject.h
//  Smashing Bricks
//
//  Created by Ronald Ram on 9/4/13.
//  Copyright (c) 2013 Ronald Ram. All rights reserved.
//

#import <Foundation/Foundation.h>

@class SKPhysicsContact;

@protocol SBIGameObject <NSObject>

- (void)didBeginContact:(SKPhysicsContact *)contact;
- (void)didEndContact:(SKPhysicsContact *)contact;
- (void)didSimulatePhysics;

@end

