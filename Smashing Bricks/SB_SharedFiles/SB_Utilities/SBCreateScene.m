//
//  SBCreateScene.m
//  Smashing Bricks
//
//  Created by Ronald Ram on 9/6/13.
//  Copyright (c) 2013 Ronald Ram. All rights reserved.
//

#import "SBCreateScene.h"
//#import <SpriteKit/SpriteKit.h>

#pragma mark IMPORT-MENU_SYSTEM HEADERS
#import "SBIntro.h"
#import "SBMainMenu.h"
#import "SBLevelSets.h"
#import "SBSettings.h"
#import "SBAbout.h"
#import "SBClassicLevels.h"
#import "SBSpaceLevels.h"
#import "SBAquaLevels.h"
#pragma mark IMPORT-GAME-LEVEL HEADERS
#import "SBClassicLevel.h"
#import "SBEndlessLevel.h"
#import "SBSpaceLevel.h"
#import "SBAquaLevel.h"


//GLOBAL Screen-Size
static SBCreateScene *screenSize_ = nil;

//GLOBAL CONST vars
NSString * const INTRO       = @"INTRO";
NSString * const MAIN_MENU   = @"MAIN_MENU";
NSString * const LEVEL_SETS  = @"LEVEL_SETS";
NSString * const SETTINGS    = @"SETTINGS";
NSString * const ABOUT       = @"ABOUT";
NSString * const CLASSIC_LS1 = @"CLASSIC_LS1";
NSString * const ENDLESS_LS2 = @"ENDLESS_LS2";
NSString * const SPACE_LS3   = @"SPACE_LS3";
NSString * const AQUA_LS4    = @"AQUA_LS4";
//
NSString * const CLASSIC_LVL = @"CLASSIC_LVL";
NSString * const ENDLESS_LVL = @"ENDLESS_LVL";
NSString * const SPACE_LVL   = @"SPACE_LVL";
NSString * const AQUA_LVL    = @"AQUA_LVL";



@implementation SBCreateScene
//
+(id)sharedInstance
{
    if ( screenSize_ == nil)
    {
        screenSize_ = [[SBCreateScene alloc] init];
        screenSize_.switchSceneTransition = [SKTransition doorsOpenHorizontalWithDuration:1.0];
    }
    return screenSize_;
}


+ (SKScene *)createScene:(NSString *)tag
{
    SBCreateScene *tmp = [SBCreateScene sharedInstance];
    CGSize screen = tmp.screenSize.bounds.size;
    
    SKScene *scene;
    
    //Load Menu...
    if ( tag == INTRO )
    {
        scene = [[SBIntro alloc] initWithSize: screen];
    }
    else if ( tag == MAIN_MENU )
    {
        scene = [[SBMainMenu alloc] initWithSize: screen];
    }
    else if ( tag == LEVEL_SETS )
    {
        scene = [[SBLevelSets alloc] initWithSize: screen];
    }
    else if ( tag == SETTINGS )
    {
        scene = [[SBSettings alloc] initWithSize: screen];
    }
    else if ( tag == ABOUT )
    {
        scene = [[SBAbout alloc] initWithSize: screen];
    }
    else if ( tag == CLASSIC_LS1 )
    {
        scene = [[SBClassicLevels alloc] initWithSize: screen];
    }
    else if ( tag == SPACE_LS3 )
    {
        scene = [[SBSpaceLevels alloc] initWithSize: screen];
    }
    else if ( tag == SPACE_LS3 )
    {
        scene = [[SBSpaceLevels alloc] initWithSize: screen];
    }
    else if ( tag == AQUA_LS4 )
    {
        scene = [[SBAquaLevels alloc] initWithSize: screen];
    }
    
    //Load Level...
    else if ( tag == CLASSIC_LVL )
    {
        scene = [[SBClassicLevel alloc] initWithSize: screen];
    }
    else if ( tag == ENDLESS_LS2 )
    {
        scene = [[SBEndlessLevel alloc] initWithSize: screen];
    }
    else if ( tag == SPACE_LVL )
    {
        scene = [[SBSpaceLevel alloc] initWithSize: screen];
    }
    else if ( tag == AQUA_LVL )
    {
        scene = [[SBAquaLevel alloc] initWithSize: screen];
    }
    //
    return scene;
}

@end
