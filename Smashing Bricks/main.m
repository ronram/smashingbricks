//
//  main.m
//  Smashing Bricks
//
//  Created by Ronald Ram on 9/3/13.
//  Copyright (c) 2013 Ronald Ram. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SBAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([SBAppDelegate class]));
    }
}
