//
//  SBCreateScene.h
//  Smashing Bricks
//
//  Created by Ronald Ram on 9/6/13.
//  Copyright (c) 2013 Ronald Ram. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <SpriteKit/SpriteKit.h>
//@class SKNode;
//@class SKScene;
//@class SKView;


//GLOBAL CONST vars
extern NSString * const INTRO;
extern NSString * const MAIN_MENU;
extern NSString * const LEVEL_SETS;
extern NSString * const SETTINGS;
extern NSString * const ABOUT;
extern NSString * const CLASSIC_LS1;
extern NSString * const ENDLESS_LS2;
extern NSString * const SPACE_LS3;
extern NSString * const AQUA_LS4;
//
extern NSString * const CLASSIC_LVL;
extern NSString * const ENDLESS_LVL;
extern NSString * const SPACE_LVL;
extern NSString * const AQUA_LVL;



@interface SBCreateScene : NSObject
//
+ (id)sharedInstance;
+ (SKScene*)createScene:(NSString*)tag;

@property SKView *screenSize;
@property SKTransition *switchSceneTransition;
//
@end
