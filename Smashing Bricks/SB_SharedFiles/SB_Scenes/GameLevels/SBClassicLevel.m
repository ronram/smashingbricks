//
//  SBClassicLevel.m
//  Smashing Bricks
//
//  Created by Ronald Ram on 9/7/13.
//  Copyright (c) 2013 Ronald Ram. All rights reserved.
//

#import "SBClassicLevel.h"
#import "SBCreateScene.h"

@interface SBClassicLevel()
@property CGFloat screenWidth;
@property CGFloat screenHeight;
@end


@implementation SBClassicLevel


#pragma mark TOUCH-EVENTS
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    for (UITouch *touch in touches)
    {
        SKNode *n = [self nodeAtPoint:[touch locationInNode:self]];
        if ([n.name isEqual: CLASSIC_LS1]) {
            [n removeAllActions];
            
            [n runAction:[SKAction waitForDuration:1.0] completion:^{
                SKScene *next = [SBCreateScene createScene:LEVEL_SETS];SKTransition *doors = [[SBCreateScene sharedInstance] switchSceneTransition];
                [self.view presentScene:next transition:doors];
            }];
        }
    }
}


#pragma mark PHYSICS-EVENTS
- (void)didBeginContact:(SKPhysicsContact *)contact
{
    //
}

- (void)didEndContact:(SKPhysicsContact *)contact
{
    //
}


#pragma mark PHYSICS-SIM FINISHED
- (void)didSimulatePhysics
{
    //
}

@end
