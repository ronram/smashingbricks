//
//  SBMainMenu.m
//  Smashing Bricks
//
//  Created by Ronald Ram on 9/3/13.
//  Copyright (c) 2013 Ronald Ram. All rights reserved.
//

#import "SBMainMenu.h"
#import "SBCreateScene.h"

@interface SBMainMenu()
@property CGFloat screenWidth;
@property CGFloat screenHeight;
@end


@implementation SBMainMenu
//
#pragma mark INIT-METHODS
- (id)initWithSize:(CGSize)size
{
    if ( self = [super initWithSize:size] )
    {
        //transfer screen size..
        self.screenWidth = self.size.width;
        self.screenHeight = self.size.height;
        
        //create Title label
        [self dataSetup];
    }
    return self;
}


#pragma mark DATA-SETUP
- (void)dataSetup
{
    //create Title label
    SKLabelNode *title = [SKLabelNode labelNodeWithFontNamed:@"Arial"];
    title.text = @"Main Menu...";
    title.position = CGPointMake(self.screenWidth *0.5, self.screenHeight *0.8);
    [self addChild: title];
    
    //LEVEL_SETS MENU
    SKSpriteNode *btnLvlSets = [[SKSpriteNode alloc] initWithColor:[SKColor greenColor] size:CGSizeMake(100, 40)];
    btnLvlSets.position = CGPointMake(self.screenWidth *0.5, self.screenHeight *0.7);
    btnLvlSets.name = LEVEL_SETS;
    [self addChild:btnLvlSets];
    
    //SETTINGS MENU
    SKSpriteNode *btnSettings = [[SKSpriteNode alloc] initWithColor:[SKColor grayColor] size:CGSizeMake(100, 50)];
    btnSettings.position = CGPointMake(self.screenWidth *0.5, self.screenHeight *0.4);
    btnSettings.name = SETTINGS;
    [self addChild:btnSettings];
    
    //ABOUT MENU
    SKSpriteNode *btnAbout = [[SKSpriteNode alloc] initWithColor:[SKColor blueColor] size:CGSizeMake(100, 40)];
    btnAbout.position = CGPointMake(self.screenWidth *0.5, self.screenHeight *0.2);
    btnAbout.name = ABOUT;
    [self addChild:btnAbout];
}


#pragma mark TOUCH-EVENTS
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    for (UITouch *touch in touches)
    {
        SKNode *n = [self nodeAtPoint:[touch locationInNode:self]];
        
        //LEVEL_SETS MENU
        if ([n.name isEqual: LEVEL_SETS]) {
            [n removeAllActions];
            
            [n runAction:[SKAction waitForDuration:1.0] completion:^{
                SKScene *next = [SBCreateScene createScene:n.name];SKTransition *doors = [[SBCreateScene sharedInstance] switchSceneTransition];
                [self.view presentScene:next transition:doors];
            }];
        }
        //SETTINGS MENU
        else if ([n.name isEqual: SETTINGS]) {
            [n removeAllActions];
            
            [n runAction:[SKAction waitForDuration:1.0] completion:^{
                SKScene *next = [SBCreateScene createScene:n.name];SKTransition *doors = [[SBCreateScene sharedInstance] switchSceneTransition];
                [self.view presentScene:next transition:doors];
            }];
        }
        //ABOUT MENU
        else if ([n.name isEqual: ABOUT]) {
            [n removeAllActions];
            
            [n runAction:[SKAction waitForDuration:1.0] completion:^{
                SKScene *next = [SBCreateScene createScene:n.name];SKTransition *doors = [[SBCreateScene sharedInstance] switchSceneTransition];
                [self.view presentScene:next transition:doors];
            }];
        }
    }//END: for-loop
}


@end


