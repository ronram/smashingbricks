//
//  SBSettings.m
//  Smashing Bricks
//
//  Created by Ronald Ram on 9/7/13.
//  Copyright (c) 2013 Ronald Ram. All rights reserved.
//

#import "SBSettings.h"
#import "SBCreateScene.h"

@interface SBSettings()
@property CGFloat screenWidth;
@property CGFloat screenHeight;
@end


@implementation SBSettings
//
#pragma mark INIT-METHODS
- (id)initWithSize:(CGSize)size
{
    if ( self = [super initWithSize:size] )
    {
        //transfer screen size..
        self.screenWidth = self.size.width;
        self.screenHeight = self.size.height;
        
        //create Title label
        [self dataSetup];
    }
    return self;
}


#pragma mark DATA-SETUP
- (void)dataSetup
{
    //create Title label
    SKLabelNode *title = [SKLabelNode labelNodeWithFontNamed:@"Arial"];
    title.text = @"Settings...";
    title.position = CGPointMake(self.screenWidth *0.5, self.screenHeight *0.8);
    [self addChild: title];
    
    //MAIN-MENU btn
    SKSpriteNode *btnBack = [[SKSpriteNode alloc] initWithColor:[SKColor yellowColor] size:CGSizeMake(100, 40)];
    btnBack.position = CGPointMake(self.screenWidth *0.5, self.screenHeight *0.3);
    btnBack.name = MAIN_MENU;
    [self addChild:btnBack];
    
}


#pragma mark TOUCH-EVENTS
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    for (UITouch *touch in touches)
    {
        SKNode *n = [self nodeAtPoint:[touch locationInNode:self]];
        if ([n.name isEqual: MAIN_MENU]) {
            [n removeAllActions];
            
            [n runAction:[SKAction waitForDuration:1.0] completion:^{
                SKScene *next = [SBCreateScene createScene:n.name];SKTransition *doors = [[SBCreateScene sharedInstance] switchSceneTransition];
                [self.view presentScene:next transition:doors];
            }];
        }
    }//END: for-loop
}

@end


