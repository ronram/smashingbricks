//
//  SBAppDelegate.h
//  Smashing Bricks
//
//  Created by Ronald Ram on 9/3/13.
//  Copyright (c) 2013 Ronald Ram. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SBAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
