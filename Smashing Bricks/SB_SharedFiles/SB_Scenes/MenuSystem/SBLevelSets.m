//
//  SBLevelSets.m
//  Smashing Bricks
//
//  Created by Ronald Ram on 9/7/13.
//  Copyright (c) 2013 Ronald Ram. All rights reserved.
//

#import "SBLevelSets.h"
#import "SBCreateScene.h"

@interface SBLevelSets()
@property CGFloat screenWidth;
@property CGFloat screenHeight;
@end



@implementation SBLevelSets
//
#pragma mark INIT-METHODS
- (id)initWithSize:(CGSize)size
{
    if ( self = [super initWithSize:size] )
    {
        //transfer screen size..
        self.screenWidth = self.size.width;
        self.screenHeight = self.size.height;
        
        //create Title label
        [self dataSetup];
    }
    return self;
}


#pragma mark DATA-SETUP
- (void)dataSetup
{
    //create Title label
    SKLabelNode *title = [SKLabelNode labelNodeWithFontNamed:@"Arial"];
    title.text = @"Level Sets...";
    title.position = CGPointMake(self.screenWidth *0.5, self.screenHeight *0.8);
    [self addChild: title];
    
    //CLASSIC-MENU btn
    SKSpriteNode *btnClassicLS = [[SKSpriteNode alloc] initWithColor:[SKColor darkGrayColor] size:CGSizeMake(40, 40)];
    btnClassicLS.position = CGPointMake(self.screenWidth *0.2, self.screenHeight *0.6);
    btnClassicLS.name = CLASSIC_LS1;
    [self addChild:btnClassicLS];
    
    //ENDLESS-MENU btn
    SKSpriteNode *btnEndlessLS = [[SKSpriteNode alloc] initWithColor:[SKColor grayColor] size:CGSizeMake(40, 40)];
    btnEndlessLS.position = CGPointMake(self.screenWidth *0.4, self.screenHeight *0.6);
    btnEndlessLS.name = ENDLESS_LS2;
    [self addChild:btnEndlessLS];
    
    //SPACE-MENU btn
    SKSpriteNode *btnSpaceLS = [[SKSpriteNode alloc] initWithColor:[SKColor grayColor] size:CGSizeMake(40, 40)];
    btnSpaceLS.position = CGPointMake(self.screenWidth *0.6, self.screenHeight *0.6);
    btnSpaceLS.name = SPACE_LS3;
    [self addChild:btnSpaceLS];

    //AQUA-MENU btn
    SKSpriteNode *btnAquaLS = [[SKSpriteNode alloc] initWithColor:[SKColor lightGrayColor] size:CGSizeMake(40, 40)];
    btnAquaLS.position = CGPointMake(self.screenWidth *0.8, self.screenHeight *0.6);
    btnAquaLS.name = AQUA_LS4;
    [self addChild:btnAquaLS];
    
    //MAIN-MENU btn
    SKSpriteNode *btnBack = [[SKSpriteNode alloc] initWithColor:[SKColor yellowColor] size:CGSizeMake(100, 40)];
    btnBack.position = CGPointMake(self.screenWidth *0.5, self.screenHeight *0.3);
    btnBack.name = MAIN_MENU;
    [self addChild:btnBack];
    
}


#pragma mark TOUCH-EVENTS
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    for (UITouch *touch in touches)
    {
        SKNode *n = [self nodeAtPoint:[touch locationInNode:self]];
        
        //CLASSIC MENU
        if ([n.name isEqual: CLASSIC_LS1]) {
            [n removeAllActions];
            
            [n runAction:[SKAction waitForDuration:1.0] completion:^{
                SKScene *next = [SBCreateScene createScene:n.name];SKTransition *doors = [[SBCreateScene sharedInstance] switchSceneTransition];
                [self.view presentScene:next transition:doors];
            }];
        }
        //ENDLESS MENU
        else if ([n.name isEqual: ENDLESS_LS2]) {
            [n removeAllActions];
            
            [n runAction:[SKAction waitForDuration:1.0] completion:^{
                SKScene *next = [SBCreateScene createScene:n.name];SKTransition *doors = [[SBCreateScene sharedInstance] switchSceneTransition];
                [self.view presentScene:next transition:doors];
            }];
        }
        //SPACE MENU
        else if ([n.name isEqual: SPACE_LS3]) {
            [n removeAllActions];
            
            [n runAction:[SKAction waitForDuration:1.0] completion:^{
                SKScene *next = [SBCreateScene createScene:n.name];SKTransition *doors = [[SBCreateScene sharedInstance] switchSceneTransition];
                [self.view presentScene:next transition:doors];
            }];
        }
        //AQUA MENU
        else if ([n.name isEqual: AQUA_LS4]) {
            [n removeAllActions];
            
            [n runAction:[SKAction waitForDuration:1.0] completion:^{
                SKScene *next = [SBCreateScene createScene:n.name];SKTransition *doors = [[SBCreateScene sharedInstance] switchSceneTransition];
                [self.view presentScene:next transition:doors];
            }];
        }
        //MAIN MENU
        else if ([n.name isEqual: MAIN_MENU]) {
            [n removeAllActions];
            
            [n runAction:[SKAction waitForDuration:1.0] completion:^{
                SKScene *next = [SBCreateScene createScene:n.name];SKTransition *doors = [[SBCreateScene sharedInstance] switchSceneTransition];
                [self.view presentScene:next transition:doors];
            }];
        }
    }//END: for-loop
}

@end


