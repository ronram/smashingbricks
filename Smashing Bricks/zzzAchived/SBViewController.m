//
//  SBViewController.m
//  Smashing Bricks
//
//  Created by Ronald Ram on 9/3/13.
//  Copyright (c) 2013 Ronald Ram. All rights reserved.
//

#import "SBViewController.h"
#import "SBCreateScene.h"
//#import "SBIntro.h"



@implementation SBViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Configure the view.
    SKView * skView = (SKView *)self.view;
    skView.showsFPS = YES;
    skView.showsNodeCount = YES;
    skView.showsDrawCount = YES;
    
    SBCreateScene *createScene = [SBCreateScene sharedInstance];
    createScene.screenSize = skView;
    
    // Create and configure the scene.
    //SKScene * scene = [SBMyScene sceneWithSize:skView.bounds.size];
    SKScene *scene = [SBCreateScene createScene:INTRO];
    scene.scaleMode = SKSceneScaleModeAspectFill;
    
    // Present the scene.
    [skView presentScene:scene];
}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        return UIInterfaceOrientationMaskAllButUpsideDown;
    } else {
        return UIInterfaceOrientationMaskAll;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

@end
